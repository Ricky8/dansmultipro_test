import requests
import random

URL = 'http://127.0.0.1:3000'
USERNAME = None
PWD = None
TOKEN = None

def test_get_using_unregister_url_to_check_status_code_equals_404():
    api_url = '{}/unregister'.format(URL)
    response = requests.get(api_url)
    assert response.status_code == 404

def test_register():
    usr = "dansmultipro{}".format(random.randint(0,10000))
    pwd = "dansmultipro123"
    api_url = "{}/auth/register".format(URL)
    body = {"username":usr,"password":pwd}
    response = requests.post(api_url, body)
    global USERNAME, PWD
    USERNAME = usr
    PWD = pwd
    assert response.status_code == 200

def test_register_with_password_less_than_6_char():
    api_url = "{}/auth/register".format(URL)
    body = {"username":"dansmultipro{}".format(random.randint(0,10000)),"password":"ikan"}
    response = requests.post(api_url, body)
    assert response.status_code == 400

def test_login():
    login_url = "{}/auth/login".format(URL)
    acc = {"username":USERNAME,"password":PWD}
    login = requests.post(login_url, json=acc)
    login_resp = login.json()
    global TOKEN
    TOKEN = login_resp["data"]["token"]
    assert login.status_code == 200

def test_login_check_wrong_passwrod():
    login_url = "{}/auth/login".format(URL)
    acc = {"username":USERNAME,"password":"123456"}
    login = requests.post(login_url, json=acc)
    assert login.status_code == 401

def test_get_position_to_check_empty_full_params():
    search_url = "{}/joblist/search".format(URL)
    headers = {"Authorization": "Bearer {}".format(TOKEN)}
    body = {"description":"","location":"","jobType":"","page":""}
    response = requests.post(search_url, json=body, headers=headers)
    resp = response.json()
    assert response.status_code == 200
    assert len(resp) == 18

def test_get_position_to_check_no_params():
    search_url = "{}/joblist/search".format(URL)
    headers = {"Authorization": "Bearer {}".format(TOKEN)}
    response = requests.post(search_url, headers=headers)
    resp = response.json()
    assert response.status_code == 200
    assert len(resp) == 18

def test_get_position_to_check_pagination():
    search_url = "{}/joblist/search".format(URL)
    headers = {"Authorization": "Bearer {}".format(TOKEN)}

    # Page 1, all data length = 18
    body = {"description":"","location":"","jobType":"","page":1}
    response = requests.post(search_url, json=body, headers=headers)
    resp = response.json()
    assert response.status_code == 200
    assert len(resp) == 10

    # Page 2, all data length = 18
    body["page"] = 2
    response2 = requests.post(search_url, json=body, headers=headers)
    resp2 = response2.json()
    assert response2.status_code == 200
    assert len(resp2) == 8

