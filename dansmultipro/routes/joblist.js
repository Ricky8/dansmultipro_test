const express = require('express')
const router = express.Router()
const logging = require('../libs/logging')
const { error } = require('../libs/utils')
const axios = require('axios')
const verify = require('../libs/verify')
const { searchByDesc, searchByLocation, searchByType, pagination } = require('../libs/helper')

const { checkSchema, validationResult } = require('express-validator')
const { detailSchema } = require('../schemas/joblistSchema')

router.post('/search', verify, async(req, res, next) => {
    try {
        const { description, location, jobType, page } = req.body
        const response = await axios.get('http://dev3.dansmultipro.co.id/api/recruitment/positions.json')
        const data = response.data
        let result = []
        if (data.length > 0)
            data.forEach(e => {
                if (searchByDesc(description || "", e) && searchByLocation(location || "", e) && searchByType(jobType || "", e))
                    result.push(e)
            })

        result = pagination(page || 0, result)

        res.status(200).send(result)
    } catch (e) {
        logging.error(`[Search error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }


})

router.get('/detail/:id', verify, checkSchema(detailSchema), async(req, res, next) => {
    try {
        const { id } = req.params
        const response = await axios.get(`http://dev3.dansmultipro.co.id/api/recruitment/positions/${id}`)
        const data = response.data

        res.status(200).send(data)
    } catch (e) {
        logging.error(`[Search error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }
})

module.exports = router