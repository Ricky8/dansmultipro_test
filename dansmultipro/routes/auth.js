const express = require('express')
const router = express.Router()
const logging = require('../libs/logging')
const { error } = require('../libs/utils')

const { checkSchema, validationResult } = require('express-validator')
const { saveData, findOneData } = require('../libs/mongo_crud')
const { registerSchema, loginSchema, refreshSchema } = require('../schemas/authSchema')

const bcrypt = require('bcryptjs')
const jwt = require("jsonwebtoken")

const tokenList = {}

router.post('/register', checkSchema(registerSchema), async(req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const encryptedPassword = await bcrypt.hash(req.body.password, 10)

        const registration = {
            username: req.body.username,
            password: encryptedPassword,
            updateAt: "",
            createAt: new Date()
        }


        const registerResult = await saveData(process.env.COL_USERS, registration)
        logging.debug(`[registerResult] ${JSON.stringify(registerResult)}`)

        res.status(200).json({
            message: 'Your account has been created'
        })
    } catch (e) {
        logging.error(`[Register error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }
})

router.post('/login', checkSchema(loginSchema), async(req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const { username, password } = req.body

        const login = await findOneData(process.env.COL_USERS, { username: username })
        logging.debug(`[Login Check] ${JSON.stringify(login)}`)

        if (login && (await bcrypt.compare(password, login.password))) {
            const token = jwt.sign({ username: username }, process.env.SECRET, { expiresIn: process.env.TOKENLIFE })
            const refreshToken = jwt.sign({ username: username }, process.env.REFRESH_TOKEN_SECRET, { expiresIn: process.env.REFRESH_TOKENLIFE })

            const response = {
                username: login.username,
                token: token,
                refreshToken: refreshToken
            }

            tokenList[refreshToken] = response

            res.status(200).send({
                data: response
            })
        } else {
            res.status(401).send({
                message: "Wrong password."
            })
        }
    } catch (e) {
        logging.error(`[Login error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }

})

router.post('/token', checkSchema(refreshSchema), async(req, res, next) => {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const { username, refreshToken } = req.body
        if ((refreshToken in tokenList) && tokenList[refreshToken].username == username) {
            const token = jwt.sign({ username: username },
                process.env.SECRET, { expiresIn: process.env.TOKENLIFE }
            )

            const response = {
                "token": token,
            }

            tokenList[refreshToken].token = token
            res.status(200).send(response)
        } else {
            res.status(404).send({
                msg: "Invalid Request"
            })
        }

    } catch (e) {
        logging.error(`[refresh token error]${JSON.stringify(e)}`)
        next(error(400, 'Something went wrong'))
    }
})

module.exports = router