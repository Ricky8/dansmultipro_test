const logging = require('./libs/logging')
const mongo = require('./libs/mongo')

const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')

require('dotenv').config()

const port = process.env.PORT

// routed
const auth = require('./routes/auth.js')
const joblist = require('./routes/joblist.js')

let config = {
    log: {
        path: "var/log/",
        level: "debug"
    }
}

logging.init({
    path: config.log.path,
    level: config.log.level
})

mongo.init({
    host: process.env.MONGO_HOST,
    port: process.env.MONGO_PORT,
    database: process.env.MONGO_DB,
    interval: process.env.MONGO_INTERVAL
})
mongo.ping((err, res) => {
    if (err) return logging.error(err.stack)

    if (!res.ok)
        return logging.error(`[Mongo] Failed to connect`)

    logging.debug(`[Mongo] Succesfully connected`)
})

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/auth', auth)
app.use('/joblist', joblist)

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({ error: err.message });
});

app.use(function(req, res) {
    res.status(404);
    res.send({ error: "Sorry, page not found" })
});

app.listen(port)
logging.info('[app] STARTED on ' + port)