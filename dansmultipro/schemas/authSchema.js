const { findOneData } = require('../libs/mongo_crud')
require('dotenv').config()

exports.registerSchema = {
    username: {
        custom: {
            options: value => {
                return findOneData(process.env.COL_USERS, { username: value })
                    .then(function(result) {
                        if (result !== null) return Promise.reject("username already registred")
                    })
            }
        }
    },
    password: {
        isLength: {
            errorMessage: 'password contain atleast 6 character',
            options: { min: 6 },
        }
    }
}

exports.loginSchema = {
    username: {
        custom: {
            options: value => {
                return findOneData(process.env.COL_USERS, { username: value })
                    .then(function(result) {
                        if (result === null) return Promise.reject("account not found")
                    })
            }
        }
    },
    password: {
        isLength: {
            errorMessage: 'password contain atleast 6 character',
            options: { min: 6 },
        }
    }
}

exports.refreshSchema = {
    refreshToken: {
        notEmpty: true,
        errorMessage: "field can not be empty"
    }
}