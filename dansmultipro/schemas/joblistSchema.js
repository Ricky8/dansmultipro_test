const { findOneData } = require('../libs/mongo_crud')
require('dotenv').config()

exports.detailSchema = {
    id: {
        notEmpty: true,
        errorMessage: "field can not be empty"
    }
}