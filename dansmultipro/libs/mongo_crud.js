const mongo = require('./mongo')

exports.saveData = function(collection, data) {
    return new Promise(function(resolve, reject) {
        mongo.insertOne(collection, data, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.findData = function(collection, doc) {
    return new Promise(function(resolve, reject) {
        mongo.find(collection, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.findOneAndUpdate = function(collection, clause, doc) {
    return new Promise(function(resolve, reject) {
        mongo.findOneAndUpdate(collection, clause, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.findDataDescending = function(collection, doc) {
    return new Promise(function(resolve, reject) {
        mongo.findDescending(collection, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.findOneData = function(collection, doc) {
    return new Promise(function(resolve, reject) {
        mongo.findOne(collection, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.updateData = function(collection, clause, doc) {
    return new Promise(function(resolve, reject) {
        mongo.updateOne(collection, clause, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}

exports.updateOneDataWithOption = function(collection, clause, doc, option) {
    return new Promise(function(resolve, reject) {
        mongo.updateOneWithOpt(collection, clause, doc, option, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}


exports.aggregateData = function(collection, doc) {
    return new Promise(function(resolve, reject) {
        mongo.aggregate(collection, doc, function(err, result) {
            if (err) reject(err)
            resolve(result)
        })
    })
}