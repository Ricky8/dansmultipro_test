'use strict'

const winston = require('winston')
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, prettyPrint, printf, colorize } = format;

process.env.TZ = 'Asia/Jakarta'

let filename = 'logging'
let path = 'var/log/'
let level = 'info'
let errorSufix = '-error'

const myFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
});

var logger = null

function createLogging(args) {
    logger = winston.createLogger({
        level: args.level,
        format: combine(
            colorize(),
            timestamp(),
            myFormat
        ),
        transports: [
            new winston.transports.File({ filename: args.path + args.filename + args.errorSufix + '.log', level: 'error' }),
            new winston.transports.File({ filename: args.path + args.filename + '.log' })
        ]
    })
}

function init(args = {}) {
    createLogging({
        filename: args.filename || filename,
        path: args.path || path,
        level: args.level || level,
        errorSufix: args.errorSufix || errorSufix
    })
}

function logInfo(message) {
    logger.info(message)
}

function logError(message) {
    logger.error(message)
}

function logDebug(message) {
    logger.debug(message)
}

function logSilly(message) {
    logger.silly(message)
}

module.exports = {
    init: init,
    info: logInfo,
    error: logError,
    debug: logDebug,
    silly: logSilly
}