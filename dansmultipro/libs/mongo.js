const logging = require('./logging')

class MongoDB {
    init(database) {
        this.MongoClient = require('mongodb').MongoClient;
        this.url = `mongodb://${database.host}:${database.port}/${database.database}`;
        this.db = database.database
        this.interval = database.interval
    }

    connect(callback = null) {
        let option = {
            useNewUrlParser: true,
            maxPoolSize: 50,
            wtimeoutMS: 2500,
            useUnifiedTopology: true
        }

        let self = this

        this.MongoClient.connect(this.url, option, function(err, client) {

            if (err) return self.reconnect(callback)

            let db = client.db()

            if (callback) {
                callback(db, client)
            }
        })

    }

    reconnect(callback = null) {
        logging.debug('[DB] Reconnecting...')

        setTimeout(() => {
            this.connect(callback)
        }, this.interval)
    }

    find(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).find(document).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    findDescending(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).find(document).sort({ createAt: -1 }).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    findOpt(collection, document, option = null, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).find(document, option).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    checkData(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).find(document).sort({ createAt: -1 }).limit(1).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    aggregate(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).aggregate(document).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    findOne(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).findOne(document, (error, result) => {
                if (callback) {
                    callback(error, result)
                }

                client.close()

            })
        })
    }

    findOneAndUpdate(collection, clause, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).findOneAndUpdate(clause, document, (error, results) => {
                if (callback) {
                    callback(error, results)
                }
                client.close()
            })
        })
    }

    findLast(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).find(document).sort({ '_id': -1 }).limit(1).toArray(function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    insertOne(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).insertOne(document, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    insertMany(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).insertMany(document, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    updateOne(collection, clause, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).updateOne(clause, document, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    updateOneWithOpt(collection, clause, document, option = {}, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).updateOne(clause, document, option, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    updateMany(collection, clause, document, option = {}, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).updateMany(clause, document, option, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    removeFields(collection, clause, document, option, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).updateOne(clause, document, option, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    replaceOne(collection, clause, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).replaceOne(clause, document, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    deleteOne(collection, document, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).deleteOne(document, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }

    bulkWrite(collection, operation, option, callback = null) {
        this.connect(function(db, client) {
            db.collection(collection).bulkWrite(operation, option, function(err, results) {
                if (callback) {
                    callback(err, results)
                }

                client.close()
            })
        })
    }


    ping(callback = null) {
        this.connect(function(db, client) {
            db.admin().ping((err, res) => {
                if (callback) {
                    callback(err, res)
                }
            })
        })
    }
}

module.exports = new MongoDB()