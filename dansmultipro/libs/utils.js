exports.error = function(status, msg) {
    var err = new Error(msg);
    err.status = status;
    return err;
}