const jwt = require("jsonwebtoken")
require('dotenv').config()

const verifyToken = (req, res, next) => {
    let token = null
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        token = req.headers.authorization.split(' ')[1]
    }

    if (token === null) {
        return res.status(401).send({
            message: "Unauthorized access."
        })
    }


    try {
        const decoded = jwt.verify(token, process.env.SECRET)
        req.user = decoded
    } catch (err) {
        return res.status(403).send({
            message: "Invalid Token"
        })
    }
    return next()
}

module.exports = verifyToken