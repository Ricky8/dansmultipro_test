exports.searchByDesc = function(str, obj) {
    if (str === "")
        return true
    else
        return obj.description.replace(/(<([^>]+)>)/ig, '').toLowerCase().split(" ").some(x => x.startsWith(str.toLowerCase()))
}

exports.searchByLocation = function(str, obj) {
    if (str === "")
        return true
    else
        return obj.location.toLowerCase().split(" ").some(x => x.startsWith(str.toLowerCase()))
}

exports.searchByType = function(type, obj) {
    if (type === "")
        return true
    if (type)
        if (obj.type.toLowerCase() == "full time")
            return true
        else
            return false
    else
        return true
}


exports.pagination = function(page, arr) {
    const len = arr.length
    let allPage
    let result = []
    if ((len > 10 && page <= 0) || (len < 10 && page <= 0))
        return arr

    if (len > 10 && page > 0)
        allPage = Math.ceil(arr.length / 10)
    if (page > allPage)
        return []
    else
        firstPage = (page * 10) - 10
    if (firstPage + 10 > arr.length)
        endPage = arr.length + 1
    else
        endPage = firstPage + 10
    result = arr.slice(firstPage, endPage)

    if (len < 10 && page > 0)
        if (page <= 1)
            return arr
        else
            return []

    return result
}