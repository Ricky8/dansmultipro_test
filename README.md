# REST API
## Installation

```bash
cd dansmultipro

npm install
```

## Usage

### Register
```bash
POST /auth/register
```

request :

```bash
{
    "username":"Foo",
    "password":"Bar"
}
```

response
```bash
{
	"message": "Your account has been created"
}
```

### Login
```bash
POST /auth/login
```

request :
```bash
{
    "username":"Foo",
    "password":"Bar"
}
```

response
```bash
{
	"data": {
		"username": "",
		"token": "",
		"refreshToken": ""
	}
}
```

### Refresh token
```bash
POST /auth/token
```

request:
```bash
{
    "username":"Foo",
    "refreshToken":"FooBar"
}
```

response:
```bash
{
    "token": ""
}
```

### Job List
```bash
POST /joblist/search
```

request:

```bash
Header
Bearer Token "access token"

Body
{
    "description":"Foo",
    "location":"Bar",
    "jobType":true,
    "page":1
}
```

response:
```bash
[
    {
        "id": "",
        "type": "",
        "url": "",
        "created_at": "",
        "company": "",
        "company_url": ,
        "location": "",
        "title": "",
        "description": "",
        "how_to_apply": "",
        "company_logo": 
    }
]
```

### Job Detail
```bash
GET /joblist/detail
```

request:
```bash
Header
Bearer Token "access token"

/joblist/detail/:id
```

response:
```bash
{
    "id": "",
    "type": "",
    "url": "",
    "created_at": "",
    "company": "",
    "company_url": ,
    "location": "",
    "title": "",
    "description": "",
    "how_to_apply": "",
    "company_logo": 
}
```

# API TEST
## Installation

```bash
apt install virtualenv

cd api_test

virtualenv test_env

```

Activate virtualenv

```bash
. test_env/bin/activate

pip install -r requirements.txt
```

Deactivate virtualenv

```bash
deactivate
```

## Usage

```bash
pytest basic_tests.py
```
